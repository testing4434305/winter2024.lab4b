import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		//Bear[] sleuth = new Bear[4];
		Bear[] sleuth = new Bear[1];
		
		for (int i = 0; i < sleuth.length; i++){
			//Bear sleuth[i] = new Bear();
			//type
			/*System.out.println("Enter a bear type (polar, grizzly, koala, etc):");
			String type = reader.nextLine();
			sleuth[i].setType(type);*/
			
			//hours of sleep
			//Useing constructor
			System.out.println("Enter how many hours the bear slept:");
			double hoursOfSleep = Double.parseDouble(reader.nextLine());
			
			//hibernation
			//Useing constructor
			System.out.println("Enter [true] if the bear is hibernatiing and [false] if it is not");
			boolean hibernation = Boolean.parseBoolean(reader.nextLine());
			
			//diet
			//Useing constructor
			System.out.println("Enter the bear's diet (carnivore, omnivore, herbivore, etc)");
			String diet = reader.nextLine();
			
			sleuth[i] = new Bear(hoursOfSleep, hibernation, diet);
		}
		//Testing the type field with no value
		System.out.println(sleuth[sleuth.length-1].getType());
		
		//Printing all the fields of the last animal
		System.out.println("Enter a bear type (polar, grizzly, koala, etc):");
		String type = reader.nextLine();
		sleuth[sleuth.length-1].setType(type);
		System.out.println(sleuth[sleuth.length-1].getType());
		System.out.println(sleuth[sleuth.length-1].getHoursOfSleep());
		System.out.println(sleuth[sleuth.length-1].getHibernation());
		System.out.println(sleuth[sleuth.length-1].getDiet());
		
		
	}
}
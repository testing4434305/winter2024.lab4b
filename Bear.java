public class Bear{
	
	//fields
	private String type;
	private double hoursOfSleep;
	private boolean hibernation;
	private String diet;
	
	//instance methods
	//Set Methods
	//Commenting all but one field
	public void setType(String oldType){
		this.type = oldType;
	}
	/*public void setHoursOfSleep(double oldHoursOfSleep){
		this.hoursOfSleep = oldHoursOfSleep;
	}
	public void setHibernation(boolean oldHibernation){
		this.hibernation = oldHibernation;
	}
	public void setDiet(String oldDiet){
		this.diet = oldDiet;
	}*/
	
	//getMethods
	public String getType(){
		return this.type;
	}
	public double getHoursOfSleep(){
		return this.hoursOfSleep;
	}
	public boolean getHibernation(){
		return this.hibernation;
	}
	public String getDiet(){
		return this.diet;
	}
	//Constructor
	public Bear(double hoursOfSleep, boolean hibernation, String diet){
		this.hoursOfSleep = 8.0;
		this.hibernation = true;
		this.diet = "omnivore";
	}
	
	/*
	 * This method evaluates the diet type and prints a message according to the diet
	 */
	public void goEat(){
		if(this.diet.equals("carnivore") || this.diet.equals("Carnivore")){
			System.out.println("This bear went fishing and caught a big fish!");
			
		} else if(this.diet.equals("omnivore") || this.diet.equals("Omnivore")){
			System.out.println("This bear is having a nice feast with berries, nuts and fish.");
			
		} else if(this.diet.equals("herbivore") || this.diet.equals("Herbivore")){
			System.out.println("This bear is munching on some nice leaves enjoying his vegan meal.");
			
		} else {
			System.out.println("This bear's diet is unknown.");
		}
	}
	
	/*
	 * Using the hoursOfSleep and hibernation fields
	 * this method prints a message according to how much the bear slept
	 */
	public void wakeUp(){
		if(this.hoursOfSleep < 8){
			if(this.hibernation){
				System.out.println("This bear is in deep sleep, nothing can wake it up now.");
			} else {
				System.out.println("This bear did nit sleep enough, he woke up in a mood.");
			}
		} else {
			if(this.hibernation){
				System.out.println("This bear woke up from hibernation and is hungry, be careful!");
			} else {
				System.out.println("This bear had a restful sleep and is ready to start the day.");
			}
		}
	}
}